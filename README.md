# Spring Boot Api #

This is an example of a Web API using Jersey.

### What is used ###

* Jersey 2.22.2
* Maven 3
* JDK 1.7
* Tomcat 8

### How to run this app ###

* Build: mvn clean package
* A war file is created in target/ folder.

### Access ##
http://localhost:8080/music-service/webapi/album