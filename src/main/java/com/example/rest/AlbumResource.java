package com.example.rest;

import java.util.List;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.repository.RepositoryAlbumImplementation;
import com.example.repository.RepositoryInterface;
import com.example.rest.domain.Album;

@Singleton
@Path("/album")
public class AlbumResource {

	RepositoryInterface<Album> repository = new RepositoryAlbumImplementation();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		List<Album> listAlbums = repository.getAll();
		GenericEntity<List<Album>> albums = new GenericEntity<List<Album>>(listAlbums) {};
        return Response.ok(albums).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") Long id) {
		return Response.ok(repository.get(id)).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		repository.delete(id);
		return Response.ok().build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)	
	public void save(Album album) {
		repository.save(album);
		Response.ok().build();
	}
}