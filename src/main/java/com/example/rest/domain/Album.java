package com.example.rest.domain;

public class Album {

	private Long id;
	private String title;
	private String artist;
	private Integer year;
	private String genre;

	public Album() {};
	
	public Album(Long id, String title, String artist, Integer year, String genre) {
		this.id = id;
		this.title = title;
		this.artist = artist;
		this.year = year;
		this.genre = genre;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	@Override
	public String toString() {
		return "[Title: " + title + 
			   " - Artist: " + artist + 
			   " - Year: " + String.valueOf(year) + 
			   " - Genre: " + genre + 
			   "]";
	}
}