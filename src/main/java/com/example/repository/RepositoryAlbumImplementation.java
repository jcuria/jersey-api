package com.example.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.rest.domain.Album;

public class RepositoryAlbumImplementation implements RepositoryInterface<Album> {
	
	Map<Long,Album> albums;

	public RepositoryAlbumImplementation() {
		albums = new HashMap<Long,Album>();
		populateAlbumRepository();
	}

	public List<Album> getAll() {
		List<Album> listAlbum = new ArrayList<Album>();
		for (Album album : albums.values()) {
			listAlbum.add(album);
		}
		return listAlbum;
	}

	public Album get(Long id) {
		return albums.get(id);
	}

	public void save(Album album) {
		albums.put(album.getId(), album);
	}

	public void delete(Long id) {
		albums.remove(id);
	}

	private void populateAlbumRepository() {
		Album album = new Album(1L, "Fear of a Blank Planet", "Porcupine Tree", 2007, "Progressive Rock");
		albums.put(album.getId(), album);
		album = new Album(2L, "Hand.Cannot.Erase.", "Steven Wilson", 2015, "Progressive Rock");
		albums.put(album.getId(), album);
		album = new Album(3L, "Blackwater Park", "Opeth", 2001, "Progressive Metal");
		albums.put(album.getId(), album);
	}
}
