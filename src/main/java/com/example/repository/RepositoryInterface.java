package com.example.repository;

import java.util.List;

public interface RepositoryInterface<T> {
	
	public T get(Long id);

	public List<T> getAll();

	public void save(T t);
	
	public void delete(Long id);
}
